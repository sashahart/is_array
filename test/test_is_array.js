var assert = require("assert");

var is_array = require("../is_array");

describe('is_array', function(){
	it('returns true for an empty array', function() {
		assert.equal(is_array([]), true);
	});
	it('returns true for an array with stuff', function() {
		assert.equal(is_array([1, 2]), true);
	});
	it('returns false for an empty object', function() {
		assert.equal(is_array({}), false);
	});
	it('returns false for a filled object', function() {
		assert.equal(is_array({a: 'b', c: 'd'}), false);
	});
	it('returns false for other junk', function() {
		assert.equal(is_array(""), false);
		assert.equal(is_array("fnord"), false);
	});
});
