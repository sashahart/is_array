module.exports = is_array;

function is_array(obj) {
    // ES5 or someone mimicking it
    if (Array.isArray != 'undefined') {
        return Array.isArray(obj);
    }
    // Fallback implementation
    if ( Object.prototype.toString.call(obj) === '[object Array]' ) {
        return true;
    }
    return false;
}
